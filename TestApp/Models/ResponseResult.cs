﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestApp.Models
{
    public class ResponseResult
    {
        public string Status { get; set; }
        public object Data { get; set; }
    }
}