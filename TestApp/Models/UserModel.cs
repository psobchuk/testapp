﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestApp.Models
{
    public class UserModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
    }
}