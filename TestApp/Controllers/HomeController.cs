﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestApp.DataLayer;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class HomeController : Controller
    {
        private UserRepository userRepository;
        private ResponseResult result;

        //inject repo or unit of work with interface here
        public HomeController()
        {
            userRepository = new UserRepository();
            result = new ResponseResult() { Status = "success", Data = "ok" };
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Check(UserModel userModel)
        {
            if(!userRepository.CheckUser(userModel.Username))
            {
                result.Data = "user exists";
                result.Status = "error";
            }
                
            return Json(result);
        }

        [HttpPost]
        public JsonResult Save(UserModel userModel)
        {
            User user = new User {
                Name = userModel.Name,
                Username = userModel.Username
            };

            int userId = userRepository.SaveUser(user);
            if(userId < 0) {
                result.Status = "error";
                result.Data = "Failed to save user";
            }

            return Json(result);

        }
    }
}
