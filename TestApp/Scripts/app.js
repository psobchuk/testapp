﻿var testApp = angular.module('testApp', []);

testApp.controller('TestCtrl', ["$scope", "$http", function ($scope, $http) {
    $scope.user = { Name: "", Username: "" };
    $scope.isValid = null;

    $scope.checkUsername = function () {
        $http.post('/home/check', $scope.user)
            .success(function (result) {
                if (result.Status == 'success') {
                    $scope.isValid = (result.Data == 'ok');
                } else {
                    $scope.isValid = false;
                    console.log(result);
                }
            })
            .error(function (error) { console.error(error); });
    };
    
    $scope.saveUser = function () {
        $http.post('/home/save', $scope.user)
            .success(function (result) {
                if (result.Status == 'success') {
                    alert('User successfully saved');
                } else {
                    alert('Failed to save user. Please try again.');
                }
            })
            .error(function (error) { console.error(error); });
    }
}]);