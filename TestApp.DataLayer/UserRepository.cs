﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.DataLayer
{
    public class UserRepository
    {
        private AppDbContext context = new AppDbContext();
        
        public User GetByUsername(string username)
        {
            try {
                var user = context.Users.Where(u => u.Username == username).FirstOrDefault();
                return user;
            } catch {
                //log data, rethrow error, etc.
                return null;
            }
        }

        public int SaveUser(User user)
        {
            int userId = -1;

            try {
                if (CheckUser(user.Username)) { 
                    user = context.Users.Add(user);
                    context.SaveChanges();
                    userId = user.Id;
                }
                
            } catch {
                //log data, rethrow error, etc.
                userId = -1;
            }

            return userId;
        }

        public bool CheckUser(string username)
        {
            var user = GetByUsername(username);
            return user == null;
        }
    }
}
