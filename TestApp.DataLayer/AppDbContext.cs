﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.DataLayer
{
    public class AppDbContext: DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}
